import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { UserModule } from '../../src/user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../src/user/entity/user.entity';
import { UserController } from '../../src/user/controller/user.controller';
import { UserService } from '../../src/user/service/user.service';
import { AuthService } from '../../src/user/service/auth.service';
import { Logger } from '@nestjs/common';
import { AppModule } from '../../src/app.module';

describe('UserController (e2e)', () => {
  let app;

  const requiredProperties = [
      'id',
      'first',
      'last',
      'email',
      'createdAt'
  ];

  beforeAll(async () => {
    const userModule: TestingModule = await Test.createTestingModule({
      imports: [
          AppModule,
          UserModule,
          TypeOrmModule.forFeature([User])
      ],
      controllers: [
          UserController,
      ],
      providers: [
          UserService,
          AuthService,
      ],
      exports: [
          TypeOrmModule
      ]
    }).compile();

    app = userModule.createNestApplication();
    await app.init();
  });

  it('/ (GET)',  async () => {
    const response = await request(app.getHttpServer())
      .get('/users')
      .expect(200);

    const data = JSON.parse(response.text);

    expect(data).toBeInstanceOf(Array);

    const first = data.shift();

    for (const property of requiredProperties) {
      expect(first).toHaveProperty(property);
    }
  });

  it('/ (POST)', async () => {
    const response = await request(app.getHttpServer())
        .post('/users/register')
        .expect(400);
  });

});
