import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppExceptionFilter } from './app.exception-filter';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { UserModule } from './user/user.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  buildApiSwagger(app)
      .useGlobalFilters(new AppExceptionFilter())
      .useGlobalPipes(new ValidationPipe());

  await app.listen(3000);
}

function buildApiSwagger(app: INestApplication): INestApplication {
  const options = new DocumentBuilder()
      .setTitle('Base backend API')
      .setDescription('Description API')
      .setVersion('1.0')
      .addTag('users')
      .addBearerAuth('AUTHORIZATION')
      .setSchemes('http')
      .setLicense('MIT', '')
      .build();
  const document = SwaggerModule.createDocument(app, options, {
    include: [UserModule],
  });
  SwaggerModule.setup('api', app, document);

  return app;
}

bootstrap();
