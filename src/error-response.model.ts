import { ApiResponseModelProperty } from '@nestjs/swagger';

export class ErrorResponseModel {
    @ApiResponseModelProperty({
      type: 'string',
      example: '2019-07-05T02:50:26.593Z'
    })
    timestamp: string = new Date().toISOString();

    @ApiResponseModelProperty({
        type: 'integer',
        example: 400,
    })
    statusCode: number;

    @ApiResponseModelProperty({
        type: 'string',
        example: 'Error message.'
    })
    message: string;
}
