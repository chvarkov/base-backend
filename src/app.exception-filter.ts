import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
    HttpStatus,
    Logger,
    BadRequestException
} from '@nestjs/common';

@Catch()
export class AppExceptionFilter implements ExceptionFilter {

    catch(exception: Error, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const response = context.getResponse();

        Logger.log(exception);

        const error = exception instanceof HttpException
            ? {
                statusCode: exception.getStatus(),
                message: exception.message.message,
            }
            : {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                message: exception.message,
            };

        response.status(error.statusCode).json({
            timestamp: new Date().toISOString(),
            ...error,
        }).send();
    }

    private handleValidationException(exception: BadRequestException) {

    }
}
