import { ApiModelProperty } from '@nestjs/swagger';
import { Equals, IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class RegisterModel {

    @IsNotEmpty()
    @MinLength(1)
    @MaxLength(255)
    @ApiModelProperty({
        required: true,
        type: 'string',
        example: 'John',
        minLength: 1,
        maxLength: 255,
        description: 'Your first name.',
    })
    readonly first: string;

    @IsNotEmpty()
    @MinLength(1)
    @MaxLength(255)
    @ApiModelProperty({
        required: true,
        type: 'string',
        example: 'Taylor',
        minLength: 1,
        maxLength: 255,
        description: 'Your last name.',
    })
    readonly last: string;

    @IsNotEmpty()
    @MinLength(4)
    @MaxLength(255)
    @IsEmail()
    @ApiModelProperty({
        required: true,
        type: 'string',
        example: 'test@domail.com',
        minLength: 4,
        maxLength: 255,
        description: 'Your email address.',
    })
    readonly email: string;

    @IsNotEmpty()
    @MinLength(1)
    @MaxLength(255)
    @Equals(this.repeatPassword, {message: 'Passwords is not equals.'})
    @ApiModelProperty({
        required: true,
        type: 'string',
        example: 'pas$w0rd',
        minLength: 6,
        maxLength: 255,
        description: 'Your password.',
    })
    readonly password: string;

    @IsNotEmpty()
    @MinLength(1)
    @MaxLength(255)
    @ApiModelProperty({
        required: true,
        type: 'string',
        example: 'pas$w0rd',
        minLength: 6,
        maxLength: 255,
        description: 'Repeat password.',
    })
    readonly repeatPassword: string;
}
