import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class CredentialsModel {

    @IsNotEmpty()
    @IsEmail()
    @MinLength(4)
    @MaxLength(255)
    @ApiModelProperty({
        required: true,
        type: 'string',
        example: 'test@domail.com',
        minLength: 4,
        maxLength: 255,
        description: 'Your email address.',
    })
    readonly email: string;

    @IsEmail()
    @MinLength(4)
    @MaxLength(255)
    @ApiModelProperty({
        required: true,
        type: 'string',
        example: 'pas$w0rd',
        minLength: 6,
        maxLength: 255,
        description: 'Your password.',
    })
    readonly password: string;
}
