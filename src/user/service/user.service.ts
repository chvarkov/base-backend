import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../entity/user.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { RegisterModel } from '../model/register.model';

@Injectable()
export class UserService {

    private saltRounds = 13;

    constructor(
        @InjectRepository(User) private userRepository: Repository<User>,
    ) {}

    async getAll(): Promise<User[]> {
        return await this.userRepository.find();
    }

    async getUserById(id: number): Promise<User> {
        return await this.userRepository.findOne(id);
    }

    async getUserByEmail(email: string): Promise<User> {
        return await this.userRepository.findOne({email})
    }

    async register(register: RegisterModel): Promise<User> {

        if (register.password !== register.repeatPassword) {
            throw new BadRequestException('Passwords is not equals.');
        }

        const user = new User();

        // user.id = 1;
        user.first = register.first;
        user.last = register.last;
        user.email = register.email;
        user.password = await this.encryptPassword(register.password);
        // user.createdAt = '2018-04-04';

        Logger.log(user);

        return await this.userRepository.save(user);
    }

    async update(id: number, user: User): Promise<UpdateResult> {
        return await this.userRepository.update(id, user);
    }

    async remove(id: number): Promise<DeleteResult> {
        return await this.userRepository.delete(id);
    }

    async encryptPassword(password: string): Promise<string> {
        return await bcrypt.hash(password, this.saltRounds)
    }
}
