// import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from './user.service';
import { CredentialsModel } from '../model/creadentials.model';

export interface JwtPayload {
    email: string;
}

@Injectable()
export class AuthService {

    constructor(
        private readonly usersService: UserService,
        // private readonly jwtService: JwtService,
    ) {}

    async authenticate(credentials: CredentialsModel): Promise<string> {

        const user = await this.usersService.getUserByEmail(credentials.email);

        if (!user) {
            throw new UnauthorizedException('User with this email does not exists.');
        }

        if (user.password !== await this.usersService.encryptPassword(credentials.password)) {
            throw new UnauthorizedException('Invalid password.');
        }

        const payload: JwtPayload = { email: 'user@email.com' };

        return 'token';
        // return this.jwtService.sign(payload);
    }
}
