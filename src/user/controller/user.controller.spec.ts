import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { Logger } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { UserService } from '../service/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../entity/user.entity';

describe('User Controller', () => {
  let module: TestingModule;
  let controller: UserController;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        AuthService,
      ],
    }).compile();
  });

  beforeEach(async () => {
    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {

    let all = controller.getAll();

    Logger.log(all);

    expect(controller).toBeDefined();
  });
});
