import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { UserService } from '../service/user.service';
import { User } from '../entity/user.entity';
import { CredentialsModel } from '../model/creadentials.model';
import { RegisterModel } from '../model/register.model';
import { ApiResponse } from '@nestjs/swagger';
import { ErrorResponseModel } from '../../error-response.model';

@Controller('users')
@ApiResponse({status: 401, type: ErrorResponseModel, description: 'Unauthorized'})
@ApiResponse({status: 400, type: ErrorResponseModel, description: 'Bad request'})
@ApiResponse({status: 404, type: ErrorResponseModel, description: 'Not found'})
export class UserController {

    constructor(
        private authService: AuthService,
        private userService: UserService,
    ) {}

    @Get()
    @ApiResponse({status: 200, type: Array<User>()})
    getAll(): Promise<User[]> {
        return this.userService.getAll();
    }

    @Get('id')
    @ApiResponse({status: 200, type: User})
    getById(@Param('id') id: number) {
        return this.userService.getUserById(id);
    }

    @Post('auth')
    async auth(@Body() body: CredentialsModel): Promise<{token: string}> {
        const token = await this.authService.authenticate(body);

        return {token};
    }

    @Post('register')
    async register(@Body() body: RegisterModel): Promise<User> {
        return await this.userService.register(body);
    }
}
