import { Module } from '@nestjs/common';
import { UserController } from './controller/user.controller';
import { AuthService } from './service/auth.service';
import { UserService } from './service/user.service';
import { User } from './entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';



@Module({
  imports: [
      TypeOrmModule.forFeature([User])
  ],
  controllers: [
      UserController,
  ],
  providers: [
    AuthService,
    UserService,
  ],
  exports: [
    AuthService,
    UserService,
  ]
})
export class UserModule {

  static errorModelType;

  static forRoot(config: ForRootConfiguration): UserModule {
    this.errorModelType = config.errorModelType;

    return this;
  }
}

export class ForRootConfiguration {
  errorModelType?:any;
}
