import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SwaggerModule } from '@nestjs/swagger';
import { ErrorResponseModel } from './error-response.model';

@Module({
  imports: [
      SwaggerModule,
      TypeOrmModule.forRoot({
          name: 'default',
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: 'root',
          database: 'base_backend',
          entities: [__dirname + '/**/*.entity{.ts,.js}'],
          synchronize: true,
      }),
      UserModule
  ],
  controllers: [
      AppController
  ], 
  providers: [AppService],
})
export class AppModule {}
